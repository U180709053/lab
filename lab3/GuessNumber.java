

import java.io.IOException;
import java.util.Random;
import java.util.Scanner;

public class GuessNumber {

	public static void main(String[] args) throws IOException {	
		Scanner reader = new Scanner(System.in); //Creates an object to read user input
		Random rand = new Random(); //Creates an object from Random class
		int number =rand.nextInt(100); //generates a number between 0 and 99
		
		
		System.out.println("Hi! I'm thinking of a number between 0 and 99.");
		System.out.print("Can you guess it: ");
		
		int guess = 0; //Read the user input
        int counter= 0;
        while (number != guess && guess != -1){
		    System.out.print("Type -1 to quit or guess another:");
            guess = reader.nextInt();
            if (number == guess)
                System.out.println("Congratulations!");
            else if (guess == -1){
                System.out.println("Sorry, the number was " + number);}          
            else if(number < guess){
                System.out.println("Sorry!");
                System.out.println("Mine is less than your guess.");}
            else if (number > guess){
                System.out.println("Sorry!");
                System.out.println("Mine is greater than your guess.");}
            counter++;
		}
        if (number == guess)
            System.out.println("Congratulations! You win after after " + counter + "attempt(s).");
        else if (guess == -1){
            System.out.println("You lose after " + counter + " attempt(s).");}
		reader.close(); //Close the resource before exiting
	}
	
	
}
